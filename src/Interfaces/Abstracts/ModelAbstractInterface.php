<?php
/**
 * Scalapay_PHP_SDK
 */
namespace Scalapay\Sdk\Interfaces\Abstracts;

/**
 * Interfaces ModelAbstractInterface
 *
 * @package Scalapay\Sdk\Interfaces\Abstracts
 */
interface ModelAbstractInterface
{
    /**
     * Converts class object to array.
     *
     * @return array
     */
    public function toArray();
}
